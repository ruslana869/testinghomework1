package com.example.tests;

import org.testng.annotations.Test;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.AssertJUnit;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.example.tests.data.SendingData;

public class SendingEmailTest extends TestBase {
	private static String path = "InputSending.xml";

	@DataProvider(name = "email")
	public static Object[] getFilter() {
		Object[] objects = null;
		try {
			File file = new File(path);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("InputData");

			objects = new Object[nodeLst.getLength()];
			for (int s = 0; s < nodeLst.getLength(); s++) {
				Node fstNode = nodeLst.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element fstElmnt = (Element) fstNode;
					SendingData data = new SendingData(
							fstElmnt.getElementsByTagName("receiver").item(0).getTextContent(),
							fstElmnt.getElementsByTagName("theme").item(0).getTextContent(),
							fstElmnt.getElementsByTagName("text").item(0).getTextContent());
					objects[s] = data;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}

	@Test(dataProvider = "email")
	public void send(SendingData data) throws Exception {
		// ��� ��������� � Assert
		String letters = driver
				.findElement(By.cssSelector(
						".m-skin-blue .b-folders .b-folders__folder_current .b-folders__folder__counters__total"))
				.getText();

		String baseUrl = Settings.getBaseUrlSending();
		manager.getNavigator().goToPage(baseUrl);
		manager.getSendingHelper().writeLetter();
		manager.getSendingHelper().sent(data);

		Assert.assertEquals(letters,
				driver.findElement(By.cssSelector(
						".m-skin-blue .b-folders .b-folders__folder_current .b-folders__folder__counters__total"))
						.getText());

	}

}
