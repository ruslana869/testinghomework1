package com.example.tests.helpers;

import org.openqa.selenium.By;

import com.example.tests.ApplicationManager;
import com.example.tests.data.DataAccount;

public class LoginHelper extends HelperBase{
//	DataAccount user=new DataAccount("pencil2017", "qwertyui6");
	public LoginHelper(ApplicationManager manager) {
		super(manager);
	}
	public void logOut(){
		if(isLoggedIn()){
			
		}
	}
	public boolean isLoggedTo(String str){
		if(driver.findElement(By.cssSelector("span.button.button_login")).getText().equals(str)){
			return true;
		};
		return false;
	}
public boolean isLoggedIn(){
	if(driver.findElements(By.cssSelector("#userButton span.button.button_login")).size()>0){
		return true;
	};
	
	return false;
}
	public void login(DataAccount user, int test) {
		
		if(isLoggedIn()){
			if(isLoggedTo(user.getLogin())){
				return;
			} 
		}	
		
		driver.findElement(By.linkText("�����")).click();
		
		driver.findElement(By.name("login")).clear();
		driver.findElement(By.name("login")).sendKeys(user.getLogin());
		driver.findElement(By.name("passwd")).clear();
		driver.findElement(By.name("passwd")).sendKeys(user.getPasswd());
		driver.findElement(By.name("twoweeks")).click();
		if(test==1){
			enterTranslate();
		}
		if(test==2){
			enterRealty();
		}
	}
	

	public void enterTranslate(){
		driver.findElement(By.cssSelector("button[type='submit']")).click();
	}

	public void enterRealty(){
		driver.findElement(By.cssSelector(".button.button_theme_action.button_size_m.i-bem.button_js_inited")).click();
	}
	
	public void logOutRealty() {
		driver.findElement(By.xpath("//td[2]/div/div[2]/div/span")).click();
		driver.findElement(By.xpath("//a[3]/i")).click();
	}
	
	
}
