package com.example.tests.helpers;

import org.openqa.selenium.By;

import com.example.tests.ApplicationManager;
import com.example.tests.data.TranslateData;

public class TranslateHelper extends HelperBase {

	public TranslateHelper(ApplicationManager manager) {
		super(manager);
	}
	public void logOutTranslate() {
		driver.findElement(By.cssSelector("span.button_login"));
		By.cssSelector("a.listbox-option:last-child");
	}
	public void findInFavourites(String word) {
		driver.findElement(By.id("favFilter")).click();
		driver.findElement(By.id("favFilterInput")).clear();
		driver.findElement(By.id("favFilterInput")).sendKeys(word);
		driver.findElement(By.cssSelector("div.fav-body")).click();
	}
public void outOfFavorites(){
	driver.findElement(By.className("button_icon_favourites2")).click();
	// find in the Favorites list word
	manager.getTranslateHelper().findInFavourites("перекресток");
	// open first meaning of this word
	driver.findElement(By.cssSelector("span.dictionary-meaning")).click();
	// put word (meaning) in favorites list
	driver.findElement(By.id("favButton")).click();
	// go back from Favorites list
	driver.findElement(By.className("button_icon_favourites2")).click();
}
	public void putToFavourites() {

		driver.findElement(By.id("textbox2")).click();
		driver.findElement(By.id("favButton")).click();
	}

	public void findWord(TranslateData data) {
		driver.findElement(By.id("textarea")).clear();
		driver.findElement(By.id("textarea")).sendKeys(data.word);
	}
}
