package com.example.tests.helpers;

import org.openqa.selenium.By;

import com.example.tests.ApplicationManager;
import com.example.tests.data.SendingData;

public class SendingEmailHelper extends HelperBase {

	public SendingEmailHelper(ApplicationManager manager) {
		super(manager);
	}
public void sent(SendingData data){
	
	fillFields("nb-37", data.getReceiver());
	fillFields("compose-subj", data.getTheme());
	fillFields("tinymce", data.getText());
	
}
	public void fillFields(String id, String text) {

		driver.findElement(By.id(id)).click();
		driver.findElement(By.id(id)).clear();
		driver.findElement(By.id(id)).sendKeys(text);
	}

	public void writeLetter() {
		driver.findElement(By.linkText("��������")).click();
		driver.findElement(By
				.xpath("//div[@id='js-page']/div/div[5]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/form/table/tbody/tr[7]/td"))
				.click();

		driver.findElement(By.cssSelector("div.b-mail-input__yabbles")).click();

	}
}
