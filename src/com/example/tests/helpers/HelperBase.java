package com.example.tests.helpers;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.example.tests.ApplicationManager;


public class HelperBase {

	protected WebDriver driver;
	protected ApplicationManager manager;

	
	public HelperBase(ApplicationManager manager){
		this.driver=manager.getDriver();
		this.manager=manager;
	}
	
}
