package com.example.tests.helpers;

import com.example.tests.ApplicationManager;

public class NavigationHelper extends HelperBase {

	public NavigationHelper(ApplicationManager manager) {
		super(manager);
	}

	public void goToPage(String baseUrl) {
		driver.get(baseUrl + "/");
	}
	
}
