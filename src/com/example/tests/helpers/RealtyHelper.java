package com.example.tests.helpers;

import org.openqa.selenium.By;

import com.example.tests.ApplicationManager;

public class RealtyHelper extends HelperBase{

	public RealtyHelper(ApplicationManager manager) {
		super(manager);
	}
	public void logOutRealty() {
		driver.findElement(By.xpath("//td[2]/div/div[2]/div/span")).click();
		driver.findElement(By.xpath("//a[3]/i")).click();
	}
	public void openSearch() {
		driver.findElement(By.xpath("(//button[@type='button'])[17]")).click();
		driver.findElement(By.xpath("//label[3]")).click();
	}

	public void filterFloors(String min, String max) {
		driver.findElement(By.id("vs-filter_floorMin")).clear();
		driver.findElement(By.id("vs-filter_floorMin")).sendKeys(min);
		driver.findElement(By.id("vs-filter_floorMax")).clear();
		driver.findElement(By.id("vs-filter_floorMax")).sendKeys(max);
	}

	public void filterArea(String min, String max) {
		driver.findElement(By.id("vs-filter_areaMax")).clear();
		driver.findElement(By.id("vs-filter_areaMax")).sendKeys(min);
		driver.findElement(By.id("vs-filter_livingSpaceMax")).clear();
		driver.findElement(By.id("vs-filter_livingSpaceMax")).sendKeys(max);
	}
	public void chooseBathRoom(){
		driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
	}
	public void favouritesAct() {

		driver.findElement(By.xpath("//button[@type='submit']")).click();
		driver.findElement(By.cssSelector("i.b-click-area")).click();
	}
	public void back(){
		driver.findElement(By.cssSelector(".search-filters__more-count")).click();
	}
}
