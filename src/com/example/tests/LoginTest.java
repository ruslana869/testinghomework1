package com.example.tests;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.xml.sax.SAXException;

import com.example.tests.data.DataAccount;

public class LoginTest extends TestBase {

	@Test
	public void loginWithValidCreditionals()
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		// create user
		String baseUrl = Settings.getBaseUrlTranslate();
		// go to link translate.yandex.ru
		manager.getNavigator().goToPage(baseUrl);
		int test = 1;
		DataAccount user = new DataAccount(Settings.getLogin(), Settings.getPasswd());
		manager.getLoginHelper().logOut();
		manager.getLoginHelper().login(user, test);
		
		Assert.assertEquals("pencil2017", driver.findElement(By.cssSelector(".button.button_login")).getText());
	}
}
