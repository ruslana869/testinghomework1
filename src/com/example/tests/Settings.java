package com.example.tests;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.xpath.XPathExpressionException;

import java.io.*;

public class Settings {
	// path
	public static String file = "Settings.xml";
	private static String baseUrl;
	private static String login;
	private static String passwd;
	private static Node first;

	public Settings() throws ParserConfigurationException, SAXException, IOException {
		prepare();
	}

	/*
	 * begin of reading
	 */
	public static void prepare() throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new File(file));
		doc.getDocumentElement().normalize();
		NodeList listOfSettings = doc.getElementsByTagName("Settings");
		first = listOfSettings.item(0);

	}

	/*
	 * read baseURL for YandexRealty
	 */
	public static String getBaseUrlRealty()
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		prepare();
		if (first.getNodeType() == Node.ELEMENT_NODE) {
			Element firstElement = (Element) first;
			NodeList firstNameList = firstElement.getElementsByTagName("BaseUrlRealty");
			Element firstNameElement = (Element) firstNameList.item(0);
			baseUrl = firstNameElement.getTextContent();
			System.out.println(baseUrl);
		}
		return baseUrl;
	}

	/*
	 * read baseURL for YandexTranslate
	 */
	public static String getBaseUrlTranslate()
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		prepare();
		if (first.getNodeType() == Node.ELEMENT_NODE) {
			Element firstElement = (Element) first;
			NodeList firstNameList = firstElement.getElementsByTagName("BaseUrlTranslate");
			Element firstNameElement = (Element) firstNameList.item(0);
			baseUrl = firstNameElement.getTextContent();
			System.out.println(baseUrl);
		}
		return baseUrl;
	}
	/*
	 * read baseURL for YandexTranslate
	 */
	public static String getBaseUrlSending()
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		prepare();
		if (first.getNodeType() == Node.ELEMENT_NODE) {
			Element firstElement = (Element) first;
			NodeList firstNameList = firstElement.getElementsByTagName("BaseUrlSending");
			Element firstNameElement = (Element) firstNameList.item(0);
			baseUrl = firstNameElement.getTextContent();
			System.out.println(baseUrl);
		}
		return baseUrl;
	}
	/*
	 * read user's login
	 */
	public static String getLogin() {

		if (first.getNodeType() == Node.ELEMENT_NODE) {
			Element firstElement = (Element) first;
			NodeList firstNameList = firstElement.getElementsByTagName("Login");
			Element firstNameElement = (Element) firstNameList.item(0);
			login = firstNameElement.getTextContent();
		}
		return login;
	}

	/*
	 * read user's password
	 */

	public static String getPasswd() {
		if (first.getNodeType() == Node.ELEMENT_NODE) {
			Element firstElement = (Element) first;
			NodeList firstNameList = firstElement.getElementsByTagName("Password");
			Element firstNameElement = (Element) firstNameList.item(0);
			passwd = firstNameElement.getTextContent();
		}
		return passwd;
	}

}
