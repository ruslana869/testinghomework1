package com.example.tests.data;

public class DataAccount {

	private String login;
	private String passwd;

	public DataAccount(String login, String passwd){
		this.setLogin(login);
		this.setPasswd(passwd);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

}
