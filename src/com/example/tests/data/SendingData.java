package com.example.tests.data;

public class SendingData {

	private String receiver;
	private String theme;
	private String text;

	public SendingData(String receiver, String theme, String text) {
		super();
		this.receiver = receiver;
		this.theme = theme;
		this.text = text;
	}

	public String getReceiver() {
		return receiver;
	}

	public String getTheme() {
		return theme;
	}

	public String getText() {
		return text;
	}
}
