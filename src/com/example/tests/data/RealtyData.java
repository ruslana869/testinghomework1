package com.example.tests.data;

public class RealtyData {

	private String minArea;
	private String maxArea;

	private String minFloor;

	private String maxFloor;

	public RealtyData(String minArea, String maxArea, String minFloor, String maxFloor) {
		this.minArea = minArea;
		this.maxArea = maxArea;
		this.minFloor = minFloor;
		this.maxFloor = maxFloor;
	}

	public String getMinArea() {
		return minArea;
	}

	public String getMaxArea() {
		return maxArea;
	}

	public String getMinFloor() {
		return minFloor;
	}

	public String getMaxFloor() {
		return maxFloor;
	}
}
