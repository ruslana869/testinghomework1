package com.example.tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

//import org.junit.*;
import org.openqa.selenium.*;
import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.example.tests.data.DataAccount;
import com.example.tests.data.TranslateData;

public class TranslateTest extends TestBase {
	private static String path = "Input.xml";

	/*
	 * read list of arguments for YandexRealty
	 */
	@DataProvider
	public static Object[] getListOfWords() {
		Object[] objects = null;
		try {
			File file = new File(path);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("InputData");
			System.out.println("Information of all employees");

			objects = new Object[nodeLst.getLength()];
			for (int s = 0; s < nodeLst.getLength(); s++) {
				Node fstNode = nodeLst.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element fstElmnt = (Element) fstNode;
					NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("Word");
					Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
					NodeList fstNm = fstNmElmnt.getChildNodes();
					// System.out.println("First Name : " + ((Node)
					// fstNm.item(0)).getNodeValue());
					TranslateData data = new TranslateData(((Node) fstNm.item(0)).getNodeValue());
					objects[s] = data;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}

	@Test(dataProvider = "getListOfWords")
	public void checkTranslation(TranslateData data) throws Exception {
		String baseUrl = Settings.getBaseUrlTranslate();
		// create user
		DataAccount user = new DataAccount(Settings.getLogin(), Settings.getPasswd());

		// go to link translate.yandex.ru
		manager.getNavigator().goToPage(baseUrl);
		// authorization
		manager.getLoginHelper().login(user, 1);
		// manager.getLoginHelper().enterTranslate();
		// find word in Yandex Translate
		manager.getTranslateHelper().findWord(data);
		manager.getTranslateHelper().putToFavourites();
		// find word in Yandex Translate
		manager.getTranslateHelper().findWord(data);
		// put word in favorites list
		manager.getTranslateHelper().putToFavourites();
		// find word in Yandex Translate
		manager.getTranslateHelper().findWord(data);
		// open Favorites list
		manager.getTranslateHelper().outOfFavorites();

		AssertJUnit.assertEquals(6, driver.findElements(By.cssSelector("span.dictionary-meaning")).size(), 0);

		manager.getTranslateHelper().logOutTranslate();
	}

	// public static void main(String[] args) {
	//
	// // YandexTranslateTest tr=new YandexTranslateTest();
	// // tr.prepare();
	// try {
	//
	// YandexTranslateTest.getListOfWords();
	// } catch (XPathExpressionException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (ParserConfigurationException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (SAXException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

}