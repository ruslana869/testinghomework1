package com.example.tests;

import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class TestBase {

	protected ApplicationManager manager;
	protected WebDriver driver;

	@Before
	public void setUp() throws Exception {
		manager = ApplicationManager.getInstance();
		driver = manager.getDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	/*
	 * ������� ���� ��� ������� ��� �����������
	 */
	public static String generateRandomString() {
		String[] words = { "����", "����������", "�������", "�����", "�����", "�����������", "�����������", "���",
				"�����", "����", "���������", "������", "���", "������", "������" };
		return words[(int) Math.random() * 10];
	}
}
