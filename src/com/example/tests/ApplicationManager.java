package com.example.tests;

import static org.junit.Assert.fail;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.xml.sax.SAXException;

import com.example.tests.helpers.LoginHelper;
import com.example.tests.helpers.NavigationHelper;
import com.example.tests.helpers.RealtyHelper;
import com.example.tests.helpers.SendingEmailHelper;
import com.example.tests.helpers.TranslateHelper;

public class ApplicationManager {

	private WebDriver driver;
	private LoginHelper loginHelper;
	private NavigationHelper navigator;
	private TranslateHelper translateHelper;
	private RealtyHelper realtyHelper;
	private SendingEmailHelper sendingHelper;
	private StringBuffer verificationErrors = new StringBuffer();

	public SendingEmailHelper getSendingHelper() {
		return sendingHelper;
	}

	public void setSendingHelper(SendingEmailHelper sendingHelper) {
		this.sendingHelper = sendingHelper;
	}

	private static ThreadLocal<ApplicationManager> app = new ThreadLocal<ApplicationManager>();

	public static ApplicationManager getInstance() {
		ApplicationManager newInstance = null;
		if (app.get() == null) {
			newInstance = new ApplicationManager();
			app.set(newInstance);
			// newInstance.navigator.goToPage(baseUrl);
		}
		return newInstance;
	}

	public TranslateHelper getTranslateHelper() {
		return translateHelper;
	}

	public RealtyHelper getRealtyHelper() {
		return realtyHelper;
	}

	public ApplicationManager() {

		driver = new FirefoxDriver();
		loginHelper = new LoginHelper(this);
		translateHelper = new TranslateHelper(this);
		realtyHelper = new RealtyHelper(this);
		navigator = new NavigationHelper(this);
	}

	public void quit() throws Exception {

		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @return the loginHelper
	 */
	public LoginHelper getLoginHelper() {
		return loginHelper;
	}

	/**
	 * @return the navigator
	 */
	public NavigationHelper getNavigator() {
		return navigator;
	}

}