package com.example.tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.*;
import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.example.tests.data.DataAccount;
import com.example.tests.data.RealtyData;

public class RealtyTest extends TestBase {

	private static String path = "InputRealty.xml";

	@DataProvider
	public static Object[] getFilter() {
		Object[] objects = null;
		try {
			File file = new File(path);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("InputData");

			objects = new Object[nodeLst.getLength()];
			for (int s = 0; s < nodeLst.getLength(); s++) {
				Node fstNode = nodeLst.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element fstElmnt = (Element) fstNode;
					RealtyData data = new RealtyData(fstElmnt.getElementsByTagName("minArea").item(0).getTextContent(),
							fstElmnt.getElementsByTagName("maxArea").item(0).getTextContent(),
							fstElmnt.getElementsByTagName("minFloor").item(0).getTextContent(),
							fstElmnt.getElementsByTagName("maxFloor").item(0).getTextContent());
					objects[s] = data;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}

	@Test(dataProvider = "getFilter()")
	public void testSearching(RealtyData data) throws Exception {
		String baseUrl = Settings.getBaseUrlRealty();
		DataAccount user = new DataAccount(Settings.getLogin(), Settings.getPasswd());
		// go to link translate.yandex.ru
		manager.getNavigator().goToPage(baseUrl);
		// authorization
		manager.getLoginHelper().login(user, 2);
		// open search box to choose parameters of flat(or house)
		manager.getRealtyHelper().openSearch();
		// filter area and floors
		manager.getRealtyHelper().filterArea(data.getMinArea(), data.getMaxArea());
		manager.getRealtyHelper().filterFloors(data.getMinFloor(), data.getMaxFloor());
		// choose type of bathroom
		manager.getRealtyHelper().chooseBathRoom();
		// add to Favorites and see
		manager.getRealtyHelper().favouritesAct();
		manager.getRealtyHelper().back();		
		AssertJUnit.assertEquals("4", driver.findElement(By.cssSelector(".search-filters__more-count")).getText());
		manager.getRealtyHelper().logOutRealty();
	}
}
