package com.example.tests;

import org.junit.Before;

import com.example.tests.data.DataAccount;

public class AuthBase extends TestBase {
	@Before
	public void setUpTest(){
		
		String baseUrl = "https://translate.yandex.ru/";	  
		// go to link translate.yandex.ru
		manager.getNavigator().goToPage(baseUrl);
		
		manager.getLoginHelper().login(new DataAccount("pencil2017", "qwertyui6"), 1);
	}
  }
